@extends('master.app')
@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
        <div class="page-header">
            <h4 class="page-title">Dashboard</h4>
        </div>
    </div>
</div>
<!-- Page Title Header Ends-->
<div class="row">
    <div class="col-md-4 grid-margin stretch-card average-price-card">
        <div class="card text-white">
            <div class="card-body">
                <div class="d-flex justify-content-between pb-2 align-items-center">
                    <h2 class="font-weight-semibold mb-0">
                        {{ $totalMahasiswa }}
                    </h2>
                    <div class="icon-holder">
                        <i class="text-white mdi mdi-account-group-outline"></i>
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <h5 class="font-weight-semibold mb-0">Total Data Mahasiswa</h5>
                    <a href="/mahasiswa" class="text-white mb-0 link">Lihat lebih lanjut</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 grid-margin stretch-card average-price-card">
        <div class="card text-white">
            <div class="card-body">
                <div class="d-flex justify-content-between pb-2 align-items-center">
                    <h2 class="font-weight-semibold mb-0">
                        {{ $totalBuku }}
                    </h2>
                    <div class="icon-holder">
                        <i class="text-white mdi mdi-book"></i>
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <h5 class="font-weight-semibold mb-0">Total Data Buku</h5>
                    <a href="/buku" class="text-white mb-0 link">Lihat lebih lanjut</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 grid-margin stretch-card average-price-card">
        <div class="card text-white">
            <div class="card-body">
                <div class="d-flex justify-content-between pb-2 align-items-center">
                    <h2 class="font-weight-semibold mb-0">
                        {{ $totalTransaksi  }}
                    </h2>
                    <div class="icon-holder">
                        <i class="text-white mdi mdi-book-open-page-variant"></i>
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <h5 class="font-weight-semibold mb-0">Total Peminjaman Buku</h5>
                    <a href="/transaksi" class="text-white mb-0 link">Lihat lebih lanjut</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
