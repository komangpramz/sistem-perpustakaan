@extends('master.app')
@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
        <div class="page-header">
            <h4 class="page-title">Update Data Mahasiswa</h4>
        </div>
    </div>
</div>


<div class="row justify-content-center">
    <div class="col-md-8 md-offset-1 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @foreach ($mahasiswa as $m)
                        <form action="/mahasiswa/updateMahasiswa" method="post">
                            {{ csrf_field() }}
                            <div class="col order-5">
                                <input type="hidden" class="form-control" id="id" name="id_mahasiswa"
                                    value="{{ $m->id_mahasiswa }}" required>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="namaMahasiswa">Nama Mahasiswa</label>
                                    <input type="text" class="form-control" id="namaMahasiswa" name="namaMahasiswa"
                                        value="{{ $m->nama }}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="nim">NIM</label>
                                    <input type="text" class="form-control" id="nim" name="nim" value="{{ $m->nim }}"
                                        required>
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email" name="email"
                                        value="{{ $m->email }}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="noTlp">No Telepon</label>
                                    <input type="text" class="form-control" id="noTlp" name="noTlp"
                                        value="{{ $m->no_tlp }}" required>
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="prodi">Prodi</label>
                                    <select class="form-control" name="prodi" id="prodi">
                                        <option selected disabled>Pilih Prodi</option>
                                        <option value="Sistem Informasi" {{ $m->prodi == 'Sistem Informasi' ? 'selected' :''}}>Sistem Informasi</option>
                                        <option value="Pendidikan Matematika" {{ $m->prodi == 'Pendidikan Matematika' ? 'selected' :''}}>Pendidikan Matematika</option>
                                        <option value="Pendidikan Bahasa Inggris" {{ $m->prodi == 'Pendidikan Bahasa Inggris' ? 'selected' :''}} >Pendidikan Bahasa Inggris</option>
                                        <option value="Pendidikan Guru Sekolah Dasar" {{ $m->prodi == 'Pendidikan Guru Sekolah Dasar' ? 'selected' :''}}>Pendidikan Guru Sekolah Dasar</option>
                                        <option value="Akuntansi" {{ $m->prodi == 'Akuntansi' ? 'selected' :''}}>Akuntansi</option>
                                        <option value="Kedokteran" {{ $m->prodi == 'Kedokteran' ? 'selected' :''}}>Kedokteran</option>
                                        <option value="Ilmu Hukum" {{ $m->prodi == 'Ilmu Hukum' ? 'selected' :''}}>Ilmu Hukum</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="jurusan">Jurusan</label>
                                    <select class="form-control" name="jurusan" id="jurusan">
                                        <option selected disabled>Pilih Jurusan</option>
                                        <option value="Teknik Informatika" {{ $m->jurusan == 'Teknik Informatika' ? 'selected' :''}}>Teknik Informatika</option>
                                        <option value="Bahasa Asing" {{ $m->jurusan == 'Bahasa Asing' ? 'selected' :''}}>Bahasa Asing</option>
                                        <option value="Ekonomi dan Akuntansi" {{ $m->jurusan == 'Ekonomi dan Akuntansi' ? 'selected' :''}}>Ekonomi dan Akuntansi</option>
                                        <option value="Kedokteran" {{ $m->jurusan == 'Kedokteran' ? 'selected' :''}}>Kedokteran</option>
                                        <option value="Hukum dan Kewarganegaraan" {{ $m->jurusan == 'Hukum dan Kewarganegaraan' ? 'selected' :''}}>Hukum dan Kewaganegaraan</option>
                                        <option value="Matematika" {{ $m->jurusan == 'Matematika' ? 'selected' :''}}>Matematika</option>
                                        <option value="Pendidikan Dasar" {{ $m->jurusan == 'Pendidikan Dasar' ? 'selected' :''}}>Pendidikan Dasar</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="fakultas">Fakultas</label>
                                    <select class="form-control" name="fakultas" id="fakultas">
                                        <option selected disabled>Pilih Fakultas</option>
                                        <option value="Fakultas Teknik dan Kejuruan" {{ $m->fakultas == 'Fakultas Teknik dan Kejuruan' ? 'selected' :''}}>Fakultas Teknik dan Kejuruan</option>
                                        <option value="Fakultas Bahasa dan Seni" {{ $m->fakultas == 'Fakultas Bahasa dan Seni' ? 'selected' :''}}>Fakultas Bahasa dan Seni</option>
                                        <option value="Fakultas Ekonomi" {{ $m->fakultas == 'Fakultas Ekonomi' ? 'selected' :''}}>Fakultas Ekonomi</option>
                                        <option value="Fakultas Kedokteran" {{ $m->fakultas == 'Fakultas Kedokteran' ? 'selected' :''}}>Fakultas Kedokteran</option>
                                        <option value="Fakultas Hukum dan Ilmu Sosial" {{ $m->fakultas == 'Fakultas Hukum dan Ilmu Sosial' ? 'selected' :''}}>Fakultas Hukum dan Ilmu Sosial</option>
                                        <option value="Fakultas Matematika dan Pendidikan Alam" {{ $m->fakultas == 'Fakultas Matematika dan Pendidikan Alam' ? 'selected' :''}}>Fakultas Matematika dan Pendidikan Alam</option>
                                        <option value="Fakultas Ilmu Pendidikan" {{ $m->fakultas == 'Fakultas Ilmu Pendidikan' ? 'selected' :''}}>Fakultas Ilmu Pendidikan</option>
                                    </select>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                        </form>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
