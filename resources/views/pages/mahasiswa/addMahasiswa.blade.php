@extends('master.app')
@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
        <div class="page-header">
            <h4 class="page-title">Tambah Data Mahasiswa</h4>
        </div>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-8 md-offset-1 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="/mahasiswa/tambahMahasiswa" method="post">
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="namaMahasiswa">Nama Mahasiswa</label>
                                    <input type="text" class="form-control" id="namaMahasiswa" name="namaMahasiswa"
                                        placeholder="Nama Mahasiswa" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="nim">NIM</label>
                                    <input type="text" class="form-control" id="nim" name="nim" placeholder="NIM"
                                        required>
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email"
                                        required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="noTlp">No Telepon</label>
                                    <input type="text" class="form-control" id="noTlp" name="noTlp"
                                        placeholder="No Telepon" required>
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="prodi">Prodi</label><br>
                                    <select class="form-control" name="prodi" id="prodi">
                                        <option selected disabled>Pilih Prodi</option>
                                        <option value="Sistem Informasi">Sistem Informasi</option>
                                        <option value="Pendidikan Matematika">Pendidikan Matematika</option>
                                        <option value="Pendidikan Bahasa Inggris">Pendidikan Bahasa Inggris</option>
                                        <option value="Pendidikan Guru Sekolah Dasar">Pendidikan Guru Sekolah Dasar</option>
                                        <option value="Akuntansi">Akuntansi</option>
                                        <option value="Kedokteran">Kedokteran</option>
                                        <option value="Ilmu Hukum">Ilmu Hukum</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="jurusan">Jurusan</label><br>
                                    <select class="form-control" name="jurusan" id="jurusan">
                                        <option selected disabled>Pilih Jurusan</option>
                                        <option value="Teknik Informatika">Teknik Informatika</option>
                                        <option value="Bahasa Asing">Bahasa Asing</option>
                                        <option value="Ekonomi dan Akuntansi">Ekonomi dan Akuntansi</option>
                                        <option value="Kedokteran">Kedokteran</option>
                                        <option value="Hukum dan Kewarganegaraan">Hukum dan Kewaganegaraan</option>
                                        <option value="Matematika">Matematika</option>
                                        <option value="Pendidikan Dasar">Pendidikan Dasar</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="fakultas">Fakultas</label>
                                    <select class="form-control" name="fakultas" id="fakultas">
                                        <option selected disabled>Pilih Fakultas</option>
                                        <option value="Fakultas Teknik dan Kejuruan">Fakultas Teknik dan Kejuruan</option>
                                        <option value="Fakultas Bahasa dan Seni">Fakultas Bahasa dan Seni</option>
                                        <option value="Fakultas Ekonomi">Fakultas Ekonomi</option>
                                        <option value="Fakultas Kedokteran">Fakultas Kedokteran</option>
                                        <option value="Fakultas Hukum dan Ilmu Sosial">Fakultas Hukum dan Ilmu Sosial</option>
                                        <option value="Fakultas Matematika dan Pendidikan Alam">Fakultas Matematika dan Pendidikan Alam</option>
                                        <option value="Fakultas Ilmu Pendidikan">Fakultas Ilmu Pendidikan</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
