@extends('master.app')
@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
        <div class="page-header">
            <h4 class="page-title">Data Mahasiswa</h4>
        </div>
    </div>
</div>

{{-- ALERT UNTUK MESSAGE FAIL --}}
@if (Session::has('fail'))
<div class="alert alert-danger" role="alert">
  {{Session::get('fail')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>    
@endif

{{-- ALERT UNTUK MESSAGE SUCCESS --}}
@if (Session::has('success'))
<div class="alert alert-success" role="alert">
  {{Session::get('success')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>    
@endif


<div class="d-flex flex-row-reverse">
    <a href="/mahasiswa/addMahasiswa" class="btn btn-sm btn-custom" role="button" aria-pressed="true">
        <i class="mdi mdi-plus" style="color: white"></i>Tambah Data
    </a>
    
</div>
<br>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th>Nama Mahasiswa</th>
        <th >NIM</th>
        <th >Email</th>
        <th >No Telepon</th>
        <th >Prodi</th>
        <th >Jurusan</th>
        <th >Fakultas</th>
        <th >Action</th>
      </tr>
    </thead>
    @foreach ($mahasiswa as $m)
    <tbody>
      <tr>
        <td>{{ $m->nama}}</td>
        <td>{{ $m->nim}}</td>
        <td>{{ $m->email}}</td>
        <td>{{ $m->no_tlp}}</td>
        <td>{{ $m->prodi}}</td>
        <td>{{ $m->jurusan}}</td>
        <td>{{ $m->fakultas}}</td>
        <td>
            <a href="/mahasiswa/editMahasiswa/{{$m->id_mahasiswa}}"><i class="mdi mdi-pencil"></i></a>
            <a href="/mahasiswa/deleteMahasiswa/{{$m->id_mahasiswa}}"onclick="return confirm('Apakah anda yakin menghapus data ini?')"><i class="mdi mdi-delete"></i></a>
        </td>
      </tr>
    </tbody>
    @endforeach
</table>
@endsection