@extends('master.app')
@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
        <div class="page-header">
            <h4 class="page-title">Update Buku Perpustakaan</h4>
        </div>
    </div>
</div>

<div class="row justify-content-center">
  <div class="col-md-8 md-offset-1 d-flex align-items-stretch grid-margin">
    <div class="row flex-grow">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            @foreach ($buku as $b)
            <form action="/buku/updateBuku" method="post">
              {{ csrf_field() }}
              <div class="col order-5">
                <input type="hidden" class="form-control" id="id" name="id_buku" value="{{ $b->id_buku }} " required>
              </div>  
                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="judulBuku">Judul Buku</label>
                      <input type="text" class="form-control" id="judulBuku" name="judulBuku" value="{{ $b->judul_buku }}" required>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="pengarang">Pengarang</label>
                      <input type="text" class="form-control" id="pengarang" name="pengarang" value="{{ $b->pengarang }}"required> 
                    </div>
                </div>
                <br>
                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="penerbit">Penerbit</label>
                      <input type="text" class="form-control" id="penerbit" name="penerbit" value="{{ $b->penerbit }}" required>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="tahunTerbit">Tahun Terbit</label>
                      <input type="date" class="form-control" id="tahunTerbit" name="tahunTerbit" value="{{ $b->tahun_terbit }}" required>
                    </div>
                </div>
                <br>
                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="tebal">Tebal Buku</label>
                      <input type="number" class="form-control" id="tebal"name="tebal" value="{{ $b->tebal }}" required>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="isbn">ISBN</label>
                      <input type="text" class="form-control" id="isbn" name="isbn" value="{{ $b->isbn}}" required>
                    </div>
                </div>
                <br>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="stokBuku">Stok Buku</label>
                    <input type="number" class="form-control" id="stokBuku" name="stokBuku" value="{{ $b->stok_buku }}" required>
                  </div>
                  <div class="form-group col-md-6">
                      <label for="tebal">Biaya Sewa Harian</label>
                      <input type="decimal" class="form-control" id="biayaSewa" name="biayaSewa" value="{{ $b->biaya_sewa_harian }}" required>
                  </div>
                </div>
            
                <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
            </form>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>                                        

@endsection