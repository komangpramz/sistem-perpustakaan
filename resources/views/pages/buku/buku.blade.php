@extends('master.app')
@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
        <div class="page-header">
            <h4 class="page-title">Data Buku Perpustakaan</h4>
        </div>
    </div>
</div>

{{-- ALERT UNTUK MESSAGE FAIL --}}
@if (Session::has('fail'))
<div class="alert alert-danger" role="alert">
  {{Session::get('fail')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>    
@endif

{{-- ALERT UNTUK MESSAGE SUCCESS --}}
@if (Session::has('success'))
<div class="alert alert-success" role="alert">
  {{Session::get('success')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>    
@endif

<div class="d-flex flex-row-reverse">
    <a href="/buku/addBuku" class="btn btn-sm btn-custom" role="button" aria-pressed="true">
        <i class="mdi mdi-plus" style="color: white"></i>Tambah Data
    </a>
    
</div>
<br>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th>Judul Buku</th>
        <th >Pengarang</th>
        <th >Penerbit</th>
        <th >Tahun Terbit</th>
        <th >Tebal</th>
        <th >ISBN</th>
        <th >Stok Buku</th>
        <th >Biaya Sewa Harian</th>
        <th >Action</th>
      </tr>
    </thead>
    @foreach ($buku as $b)
    <tbody>
      <tr>
        <td>{{ $b->judul_buku}}</td>
        <td>{{ $b->pengarang}}</td>
        <td>{{ $b->penerbit}}</td>
        <td>{{ date('d M Y', strtotime($b->tahun_terbit))}}</td>
        <td>{{ $b->tebal}} Halaman</td>
        <td>{{ $b->isbn}}</td>
        <td>{{ $b->stok_buku}}</td>
        <td>Rp. {{ $b->biaya_sewa_harian}}</td>
        <td>
            <a href="/buku/editBuku/{{$b->id_buku}}"><i class="mdi mdi-pencil"></i></a>
            <a href="/buku/deleteBuku/{{$b->id_buku}}" onclick="return confirm('Apakah anda yakin menghapus data ini?')"><i class="mdi mdi-delete"></i></a>
        </td>
      </tr>
    </tbody>
    @endforeach
</table>
@endsection