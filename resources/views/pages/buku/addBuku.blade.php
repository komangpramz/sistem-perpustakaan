@extends('master.app')
@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
        <div class="page-header">
            <h4 class="page-title">Tambah Buku Perpustakaan</h4>
        </div>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-8 md-offset-1 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="/buku/tambahBuku" method="post">
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="judulBuku">Judul Buku</label>
                                    <input type="text" class="form-control" id="judulBuku" name="judulBuku"
                                        placeholder="Judul Buku" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="pengarang">Pengarang</label>
                                    <input type="text" class="form-control" id="pengarang" name="pengarang"
                                        placeholder="Pengarang" required>
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="penerbit">Penerbit</label>
                                    <input type="text" class="form-control" id="penerbit" name="penerbit"
                                        placeholder="Penerbit" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="tahunTerbit">Tahun Terbit</label>
                                    <input type="date" class="form-control" id="tahunTerbit" name="tahunTerbit"
                                        placeholder="Tahun Terbit" required>
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="tebal">Tebal Buku</label>
                                    <input type="number" class="form-control" id="tebal" name="tebal"
                                        placeholder="Tebal Buku" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="isbn">ISBN</label>
                                    <input type="text" class="form-control" id="isbn" name="isbn" placeholder="ISBN"
                                        required>
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="stokBuku">Stok Buku</label>
                                    <input type="number" class="form-control" id="stokBuku" name="stokBuku"
                                        placeholder="Stok Buku" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="tebal">Biaya Sewa Harian</label>
                                    <input type="number" class="form-control" id="biayaSewa" name="biayaSewa"
                                        placeholder="Biaya Sewa Harian" required>
                                </div>

                                
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
