@extends('master.app')
@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
        <div class="page-header">
            <h4 class="page-title">Tambah Transaksi Peminjaman Buku</h4>
        </div>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-8 md-offset-1 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="insertTransaksi" method="post" class="form-sample">
                            {{ csrf_field() }}                            
                            <div class="form-group">
                                <label for="id_mahasiswa">Mahasiswa Peminjam</label>
                                <select required name="id_mahasiswa" class="form-control">
                                    <option value="" selected disabled>Pilih Mahasiswa</option>
                                    @foreach ($mahasiswa as $m)                    
                                        <option value="{{ $m->id_mahasiswa }}">{{ $m->nama}} - {{ $m->nim }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="id_buku">Judul Buku</label>
                                <select required name="id_buku" class="form-control">
                                    <option value="" selected disabled>Pilih Judul Buku</option>
                                    @foreach ($buku as $b)                    
                                        @if ($b->stok_buku > 0)
                                            <option value="{{ $b->id_buku }}">{{ $b->judul_buku}}</option>                        
                                        @endif
                                    @endforeach                
                                </select>
                            </div>                                                                                    
                            <div class="form-group">
                                <label for="tanggal_pinjam">Tanggal Peminjaman</label>
                                <input type="date" class="form-control date-control" id="tanggal_pinjam" name="tanggal_pinjam"
                                    placeholder="Tanggal Peminjaman" required>
                            </div>                                                                 
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>
@endsection
