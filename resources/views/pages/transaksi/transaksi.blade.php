@extends('master.app')
@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
        <div class="page-header">
            <h4 class="page-title">Data Transaksi Peminjaman Buku</h4>
        </div>
    </div>
</div>

{{-- ALERT UNTUK MESSAGE FAIL --}}
@if (Session::has('fail'))
<div class="alert alert-danger" role="alert">
  {{Session::get('fail')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>    
@endif

{{-- ALERT UNTUK MESSAGE SUCCESS --}}
@if (Session::has('success'))
<div class="alert alert-success" role="alert">
  {{Session::get('success')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>    
@endif

<div class="d-flex flex-row-reverse">
    <a href="/transaksi/createTransaksi" class="btn btn-sm btn-custom" role="button" aria-pressed="true">
        <i class="mdi mdi-plus" style="color: white"></i>Buat Transaksi
    </a>
    
</div>
<br>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th >ID Transaksi</th>
        <th >Peminjam</th>
        <th >Judul Buku</th>
        <th >Tanggal Pinjam</th>
        <th >Tanggal Pengembalian</th>        
        <th >Biaya Peminjaman</th>
        <th >Status Peminjaman</th>        
        <th >Action</th>
      </tr>
    </thead>
    @foreach ($transaksi as $t)
    <tbody>
      <tr>
        <td>{{ $t->id_transaksi}}</td>
        <td>{{ $t->nama}}</td>
        <td>{{ $t->judul_buku}}</td>
        <td>{{ date('d M Y', strtotime($t->tanggal_pinjam))}}</td>
        <td>
            @if ($t->tanggal_kembali  == NULL)
                <span class="btn btn-danger btn-none">Belum Dikembalikan</span>     
            @else
            {{ date('d M Y', strtotime($t->tanggal_kembali))}}
            @endif              
        </td>        
        <td>Rp. {{ $t->total_biaya}}</td>
        <td>            
            @if ($t->status_pinjam == '1')
                <span class="btn btn-warning btn-none">Dipinjam</span>
            @elseif ($t->status_pinjam == '0')
            <span class="btn btn-success btn-none">Dikembalikan</span>
            @endif            
        </td>
        <td>               
            @if ($t->status_pinjam == '1')
            <a href="transaksi/editTransaksi/{{$t->id_transaksi}}"><i class="mdi mdi-pencil"></i></a>
            @endif                     
            @if ($t->status_pinjam == '0')            
            <a href="transaksi/deleteTransaksi/{{$t->id_transaksi}}"onclick="return confirm('Apakah anda yakin menghapus data ini?')"><i class="mdi mdi-delete"></i></a>
            @endif
        </td>
      </tr>
    </tbody>
    @endforeach
</table>
@endsection