@extends('master.app')
@section('content')
<!-- Page Title Header Starts-->
<div class="row page-title-header">
    <div class="col-12">
        <div class="page-header">
            <h4 class="page-title">Edit Transaksi Peminjaman Buku</h4>
        </div>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-8 md-offset-1 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                @foreach ($transaksi as $t)                                  
                <div class="card">
                    <div class="card-body">
                        <form action="../updateTransaksi" method="post" class="form-sample">
                            {{ csrf_field() }}      
                            <input type="hidden" class="form-control" name="id_transaksi" value="{{ $t->id_transaksi }}" required>
                            <div class="form-group">
                                <label for="tanggal_pinjam">Tanggal Peminjaman</label>
                                <input readonly type="date" class="form-control date-control" id="tanggal_pinjam" name="tanggal_pinjam"
                                    placeholder="Tanggal Peminjaman" required value="{{ date('Y-m-d', strtotime($t->tanggal_pinjam)) }}">
                            </div>                       
                            <div class="form-group">
                                <label for="id_mahasiswa">Mahasiswa Peminjam</label>
                                <select required name="id_mahasiswa" class="form-control">
                                    <option value="" selected disabled>Pilih Mahasiswa</option>
                                    @foreach ($mahasiswa as $m)                    
                                        <option  {{ $m->id_mahasiswa == $t->id_mahasiswa ? 'selected' : '' }} value="{{ $m->id_mahasiswa }}">{{ $m->nama}} - {{ $m->nim }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="id_buku">Judul Buku</label>
                                <select required name="id_buku" class="form-control">
                                    <option value="" selected disabled>Pilih Judul Buku</option>
                                    @foreach ($buku as $b)                    
                                        @if ($b->stok_buku > 0)
                                            <option {{ $b->id_buku == $t->id_buku ? 'selected' : '' }} value="{{ $b->id_buku }}">{{ $b->judul_buku}}</option>                        
                                        @endif
                                    @endforeach                
                                </select>
                            </div>                                                                                                                  
                            <div class="form-group">
                                <label for="status_pinjam">Status Peminjaman</label>
                                <select class="form-control" name="status_pinjam">
                                    <option {{ $t->status_pinjam == '0' ? 'selected' : ''}} value="0">Dikembalikan</option>
                                    <option {{ $t->status_pinjam == '1' ? 'selected' : ''}} value="1">Dipinjam</option>
                                </select>                                
                                <span class="text-note text-danger">Note: Mengubah status pinjam menjadi "Dikembalikan" berarti Anda tidak akan dapat mengubah data transaksi lagi</span>
                            </div>                                                               
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                    </div>
                </div>
                @endforeach
            </div>
        </div>        
    </div>
</div>
@endsection
