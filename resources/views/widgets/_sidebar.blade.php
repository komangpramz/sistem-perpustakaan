<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        {{-- <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
                <div class="profile-image">
                    <img class="img-xs rounded-circle" src="assets/images/faces/face8.jpg" alt="profile image">
                    <div class="dot-indicator bg-success"></div>
                </div>
                <div class="text-wrapper">
                    <p class="profile-name">Allen Moreno</p>
                    <p class="designation">Premium user</p>
                </div>
            </a>
        </li> --}}
        <br>
        <li class="nav-item nav-category">Menu Utama</li>
        <li class="nav-item">
            <a class="nav-link" href="/">
                <i class="menu-icon typcn typcn-home"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>        
        <li class="nav-item">
            <a class="nav-link" href="/mahasiswa">
                <i class="menu-icon typcn typcn-group-outline"></i>
                <span class="menu-title">Mahasiswa</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/buku">
                <i class="menu-icon typcn typcn-th-large-outline"></i>
                <span class="menu-title">Buku</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/transaksi">
                <i class="menu-icon typcn typcn-bell"></i>
                <span class="menu-title">Transaksi</span>
            </a>
        </li>
    </ul>
</nav>
