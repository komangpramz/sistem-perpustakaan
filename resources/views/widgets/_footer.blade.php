<footer class="footer">
    <div class="container-fluid clearfix">
      <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Sistem Informasi Manajemen Perpustakaan Kampus</span>
      <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Ujian Tengah Semester Pemrograman Berbasis Framework 2021</span>
    </div>
  </footer>