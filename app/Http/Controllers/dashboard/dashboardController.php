<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class dashboardController extends Controller
{
    public function viewDashboard(){
        $totalMahasiswa = DB::table('mahasiswa')->count();
        $totalBuku = DB::table('buku')->count();
        $totalTransaksi = DB::table('transaksi')->count();

        return view(
            'pages.dashboard.dashboard',
            [
                'totalMahasiswa' => $totalMahasiswa,
                'totalBuku' => $totalBuku,
                'totalTransaksi' => $totalTransaksi
            ]
        );
    }
}
