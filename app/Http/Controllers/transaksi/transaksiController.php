<?php

namespace App\Http\Controllers\transaksi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Carbon\Carbon;

class transaksiController extends Controller
{
    public function viewDataTransaksi() {
        $transaksi = DB::table('transaksi')
        ->join('mahasiswa', 'transaksi.id_mahasiswa', '=', 'mahasiswa.id_mahasiswa')
        ->join('buku', 'transaksi.id_buku', '=', 'buku.id_buku')
        ->select('transaksi.*', 'mahasiswa.nama', 'buku.judul_buku')
        ->orderBy('id_transaksi', 'asc')
        ->get();    
        return view('pages.transaksi.transaksi', ['transaksi' => $transaksi]);
    }

    public function createTransaksi(){
        $mahasiswa = DB::table('mahasiswa')->get();
        $buku = DB::table('buku')->get();
        return view('pages.transaksi.createTransaksi', 
            [
                'mahasiswa' => $mahasiswa, 
                'buku' => $buku
            ]
        );
    }

    public function insertTransaksi(Request $request){
        $id_mhs = $request->id_mahasiswa;
        $id_buku = $request->id_buku;
        $tanggal_pinjam = $request->tanggal_pinjam;
        $date_pinjam = date_create($request->tanggal_pinjam);  
        $lama_pinjam = $request->lama_pinjam;        
        $status_pinjam = '1';
        $total_biaya = '0';           
        
        $stok_buku = DB::table('buku')->select('stok_buku')->where('id_buku', $id_buku)->get();        

        if($stok_buku[0]->stok_buku > 1) {
            DB::table('transaksi')->insert([
                'id_mahasiswa' => $id_mhs,
                'id_buku' => $id_buku,
                'tanggal_pinjam' => $tanggal_pinjam,                  
                'status_pinjam' => $status_pinjam,
                'total_biaya' => $total_biaya
            ]);    
            
            return redirect('/transaksi')->with('success','Data Transaksi Berhasil ditambah!');
        } else {
            return redirect('/transaksi')->with('fail','Transaksi Gagal, Stok Buku Kurang!');
        }        
    }   

    public function editTransaksi($id_transaksi){
        $mahasiswa = DB::table('mahasiswa')->get();
        $buku = DB::table('buku')->get();
        $transaksi = DB::table('transaksi')
        ->join('mahasiswa', 'transaksi.id_mahasiswa', '=', 'mahasiswa.id_mahasiswa')
        ->join('buku', 'transaksi.id_buku', '=', 'buku.id_buku')
        ->select('transaksi.*', 'mahasiswa.*', 'buku.*')       
        ->where('transaksi.id_transaksi', $id_transaksi)
        ->get();
        return view('pages.transaksi.editTransaksi',
            [
                'transaksi'=>$transaksi,
                'mahasiswa'=>$mahasiswa,
                'buku'=>$buku
            ]
        );
    }

    public function updateTransaksi(Request $request){
        $id_transaksi = $request->id_transaksi;
        $id_mhs = $request->id_mahasiswa;
        $id_buku = $request->id_buku;               
        $status_pinjam = $request->status_pinjam;
        $tanggal_pinjam = $request->tanggal_pinjam;   
        
        if($status_pinjam == '1') {
            DB::table('transaksi')->where('id_transaksi', $id_transaksi)->update([
                'id_mahasiswa' => $id_mhs,
                'id_buku' => $id_buku,
            ]);

            return redirect('/transaksi')->with('success','Data Transaksi Berhasil di Update!');

        }elseif($status_pinjam == '0'){

            // Mencari Interval Peminjaman
            $tanggal_pinjam = new DateTime($tanggal_pinjam);
            $tanggal_kembali =  new DateTime();  
            $lama_pinjam = $tanggal_kembali->diff($tanggal_pinjam)->format('%a');
            // Menghitung Total Biaya Sewa
            $total_biaya = $this->getTotalSewa($lama_pinjam, $id_buku);            
                
            DB::table('transaksi')->where('id_transaksi', $id_transaksi)->update([
                'id_mahasiswa' => $id_mhs,
                'id_buku' => $id_buku,
                'tanggal_kembali' => $tanggal_kembali->format('Y-m-d'),
                'status_pinjam' => '0',
                'total_biaya' => $total_biaya
            ]);

            return redirect('/transaksi')->with('success','Status Transaksi Berhasil di Update!');

        }
    }

    // Function mencari total biaya sewa
    public static function getTotalSewa($lama_pinjam, $id_buku) {
        $buku = DB::table('buku')->where('id_buku', '=', $id_buku)->get();
        $biaya_sewa = $buku[0]->biaya_sewa_harian;        
        $total_sewa = $biaya_sewa*$lama_pinjam;
        return $total_sewa ;
    }

    public function deleteTransaksi($id_transaksi) {
        DB::table('transaksi')->where('id_transaksi',$id_transaksi)->delete();

        return redirect('/transaksi')->with('success','Data Transaksi Berhasil dihapus!');
    }
    
}
