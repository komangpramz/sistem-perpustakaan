<?php

namespace App\Http\Controllers\mahasiswa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class mahasiswaController extends Controller
{
    public function showAddMahasiswa(){
        return view('pages.mahasiswa.addMahasiswa');
    }
    public function viewDataMahasiswa(){
        $mahasiswa= DB::table('mahasiswa')-> get();
        return view('pages.mahasiswa.mahasiswa', ['mahasiswa'=>$mahasiswa]);
    }

    public function simpanDataMahaiswa(Request $request){

        DB::table('mahasiswa')->insert(
            [
                'nama'=>$request->namaMahasiswa, 
                'nim'=>$request->nim, 
                'email'=>$request->email, 
                'no_tlp'=>$request->noTlp , 
                'prodi'=>$request->prodi, 
                'jurusan'=>$request->jurusan,
                'fakultas'=>$request->fakultas,
            ]
        );
        return redirect('/mahasiswa')->with('success','Berhasil Menambahkan Data Mahasiswa!');  

       // return redirect('/mahasiswa')
       //  -> with('status', 'Data Mahasiswa Berhasil Ditambahkan');

    }

    public function editMahasiswa($id_mahasiswa){
        $mahasiswa = DB::table('mahasiswa')-> where('id_mahasiswa',$id_mahasiswa)->get();
        return view('pages.mahasiswa.editMahasiswa',['mahasiswa'=>$mahasiswa]);
    }

    public function updateMahasiswa(Request $request){
        $mahasiswa= DB::table('mahasiswa')->where('id_mahasiswa', $request->id_mahasiswa)->update([
                'nama'=>$request->namaMahasiswa, 
                'nim'=>$request->nim, 
                'email'=>$request->email, 
                'no_tlp'=>$request->noTlp , 
                'prodi'=>$request->prodi, 
                'jurusan'=>$request->jurusan,
                'fakultas'=>$request->fakultas, 
        ]);

        return redirect('/mahasiswa')->with('success','Data Mahasiswa Berhasil di Update!');

        //return redirect ('/mahasiswa')
        // -> with('status', 'Data Mahasiwa Berhasil Diupdate');
    }

    public function hapusMahasiswa($id_mahasiswa){
        try {
            DB::table('mahasiswa')->where('id_mahasiswa',$id_mahasiswa)->delete();
            
            return redirect('/mahasiswa')->with('success','Data Mahasiswa Berhasil di Hapus!');


        }catch (\Illuminate\Database\QueryException $e) {
            if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                return redirect()->back()->with('fail','Mahasiswa Tidak Bisa Dihapus, Karena Data Mahasiswa Masih Ada pada Data Transaksi!');
            }
        }          
    }
}
