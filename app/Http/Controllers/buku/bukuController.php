<?php

namespace App\Http\Controllers\buku;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class bukuController extends Controller
{
    public function showAddBuku(){
        return view('pages.buku.addBuku');
    }
    public function viewDataBuku(){
        $buku= DB::table('buku')-> get();
        return view('pages.buku.buku', ['buku'=>$buku]);
    }

    public function simpanDataBuku(Request $request){
        DB::table('buku')->insert(
            [
                'judul_buku'=>$request->judulBuku, 
                'pengarang'=>$request->pengarang, 
                'penerbit'=>$request->penerbit, 
                'tahun_terbit'=>$request->tahunTerbit , 
                'tebal'=>$request->tebal, 
                'isbn'=>$request->isbn,
                'stok_buku'=>$request->stokBuku,
                'biaya_sewa_harian'=>$request->biayaSewa,   
            ]
        );      
        
        return redirect('/buku')->with('success','Berhasil Menambahkan Data Buku!');  

      // return redirect('/buku');
        // -> with('status', 'Data Karyawan Berhasil Ditambahkan');
    }

    public function editBuku($id_buku){
        $buku = DB::table('buku')-> where('id_buku',$id_buku)->get();
        return view('pages.buku.editBuku',['buku'=>$buku]);
    }

    public function updateBuku(Request $request){
        $buku= DB::table('buku')->where('id_buku', $request->id_buku)->update([
                'judul_buku'=>$request->judulBuku, 
                'pengarang'=>$request->pengarang, 
                'penerbit'=>$request->penerbit, 
                'tahun_terbit'=>$request->tahunTerbit , 
                'tebal'=>$request->tebal, 
                'isbn'=>$request->isbn,
                'stok_buku'=>$request->stokBuku,
                'biaya_sewa_harian'=>$request->biayaSewa,  
        ]);

        return redirect('/buku')->with('success','Data Buku Berhasil di Update!');

       // return redirect ('/buku');
        // -> with('status', 'Data Karyawan Berhasil Diupdate');
    }

    public function hapusBuku($id_buku){
        try {
            DB::table('buku')->where('id_buku',$id_buku)->delete();

             return redirect('/buku')->with('success','Data Buku Berhasil di Hapus!');

        }catch (\Illuminate\Database\QueryException $e) {
            if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                return redirect()->back()->with('fail','Buku Tidak Bisa Dihapus, Karena Data Buku Masih Ada pada Data Transaksi!');
            }
        }        
    }
}
