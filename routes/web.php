<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ROUTE INDEX
Route::get('/', [App\Http\Controllers\dashboard\dashboardController::class,'viewDashboard']);

// ROUTE BUKU
Route::prefix('buku')->group(function(){
    Route::get('/',[App\Http\Controllers\buku\bukuController::class,'viewDataBuku']);
    Route::get('addBuku',[App\Http\Controllers\buku\bukuController::class,'showAddBuku']);
    Route::post('tambahBuku',[App\Http\Controllers\buku\bukuController::class,'simpanDataBuku']);
    Route::get('editBuku/{id_buku}', [App\Http\Controllers\buku\bukuController::class, 'editBuku']);
    Route::post('updateBuku', [App\Http\Controllers\buku\bukuController::class, 'updateBuku']);
    Route::get('deleteBuku/{id_buku}', [App\Http\Controllers\buku\bukuController::class, 'hapusBuku']);
});

// ROUTE MAHASISWA
Route::prefix('mahasiswa')->group(function(){
    Route::get('/',[App\Http\Controllers\mahasiswa\mahasiswaController::class,'viewDataMahasiswa']);
    Route::get('addMahasiswa',[App\Http\Controllers\mahasiswa\mahasiswaController::class,'showAddMahasiswa']);
    Route::post('tambahMahasiswa',[App\Http\Controllers\mahasiswa\mahasiswaController::class, 'simpanDataMahaiswa']);
    Route::get('editMahasiswa/{id_mahasiswa}', [App\Http\Controllers\mahasiswa\mahasiswaController::class, 'editMahasiswa']);
    Route::post('updateMahasiswa', [App\Http\Controllers\mahasiswa\mahasiswaController::class, 'updateMahasiswa']);
    Route::get('deleteMahasiswa/{id_mahasiswa}', [App\Http\Controllers\mahasiswa\mahasiswaController::class, 'hapusMahasiswa']);
});

// ROUTE TRANSAKSI
Route::prefix('transaksi')->group(function(){
    Route::get('/', [App\Http\Controllers\transaksi\transaksiController::class,'viewDataTransaksi']);
    Route::get('createTransaksi', [App\Http\Controllers\transaksi\transaksiController::class,'createTransaksi']);
    Route::post('insertTransaksi', [App\Http\Controllers\transaksi\transaksiController::class,'insertTransaksi']);
    Route::get('editTransaksi/{id_transaksi}', [App\Http\Controllers\transaksi\transaksiController::class,'editTransaksi']);
    Route::post('updateTransaksi', [App\Http\Controllers\transaksi\transaksiController::class,'updateTransaksi'])->name('transaksi.update');    
    Route::get('deleteTransaksi/{id_transaksi}',[App\Http\Controllers\transaksi\transaksiController::class,'deleteTransaksi']);
});